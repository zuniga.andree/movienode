const fs = require("fs");
const path = require("path");
const Mongoose = require("mongoose");
const basename = path.basename(__filename);
const { DB } = require("../../config/enviroments");
const config = DB;
const db = {};

const mongoose = Mongoose.connect(config.URI)
  .then(db => console.log('DB is conected to ', db.connection.host))
  .catch(err => console.error(err))

const movieModel = require('./movie');

db.movieModel = movieModel

db.mongoose = mongoose;
db.Mongoose = Mongoose;
module.exports = db;
