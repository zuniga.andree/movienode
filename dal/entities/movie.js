const { Schema, model } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const movieSchema = new Schema({
    title: {
        type: String,
        required: true, 
        unique: true
    },
    year: Number,
    released: Date,
    genre: String,
    director: String,
    actors: String,
    plot: String,
    ratings: []
})

movieSchema.plugin(mongoosePaginate);

module.exports = model('movie', movieSchema, 'movies')

