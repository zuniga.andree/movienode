module.exports = {
    toDbEntity(movie){
        const { title, year, released, genre, director, actors, plot, ratings } = movie;
        return { title, year, released, genre, director, actors, plot, ratings }
    }
}