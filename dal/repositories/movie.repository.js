class MovieRepository {
    constructor({ db, logger, config }) {
        this._db = db;
        this._logger = logger;
        this._config = config;
    }

    async getMovies(pageSelect) {
        const {page} = pageSelect
        console.log(page)
        return await this._db.movieModel.paginate({},{limit:this._config.PAGINATION_LIMIT,page:page});
    }

    async getMovie(filter) {
        return await this._db.movieModel.findOne(filter)
    }

    async createMovie(movie) {
        const movieDB = new this._db.movieModel({
            ...movie
        })

        return await movieDB.save();
    }
}

module.exports = MovieRepository;