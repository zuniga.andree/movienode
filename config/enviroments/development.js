module.exports={
    PORT: 5000,
    DB: {
        URI: 'mongodb://mongodb/moviedb'
    },
    PAGINATION_LIMIT: 5
}