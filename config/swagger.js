const swaggerJsdoc = require('swagger-jsdoc');
const path = require('path');

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'MovieNode Challenge!',
            version: '1.0.0',
            contact: {
                name: 'Andree Zuñiga',
                email: 'zuniga.andree@gmail.com',
            },
        },
        servers: [{
            url: 'http://localhost:5000/api',
        }],

    },
    apis: [path.resolve(__dirname,'../api/documentation/*yaml')]
};

module.exports = swaggerJsdoc(options);