class UserController {

    constructor({ MovieService }) {
        this._movieService = MovieService;
    }

    async getMovies(pageSelect) {
        return await this._movieService.getMovies(pageSelect);
    }

    async getMovie(params) {
        return await this._movieService.getMovie(params);
    }

    async createMovie(movie) {
        const createdMovie = await this._movieService.createMovie(movie);

        return {
            payload: createdMovie
        }
    }

    async findAndReplace(movieReplace) {
        const createdMovie = await this._movieService.findAndReplace(movieReplace);

        return {
            plotReplaced: createdMovie
        }
    }
}

module.exports = UserController;