const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('koa2-cors');



class Server {
    constructor({ config, router, logger}) {
        this._config = config;
        this._koa = new Koa();
        this._logger = logger;
        this._koa.use(cors());
        this._koa.use(bodyParser());
        this._koa.use(router.routes()).use(router.allowedMethods);
    }

    start() {
        return new Promise((resolve, reject) => {
            const http = this._koa.listen(this._config.PORT, () => {
                const { port } = http.address();
                this._logger.info('Application running on port ' + port);
            })
        })
    }
}

module.exports = Server;