const {asClass,createContainer,asFunction,asValue} = require('awilix');

//app start
const StartUp=require('./startup')
const Server=require('./server')
const config = require('../config/enviroments')
const logger = require('./util/logger')

//routes
const Routes = require('../api/routes')
const MovieRoutes = require('./routes/movie.routes')

//controllers
const {MovieController} = require('../api/controllers')

//services
const {MovieServices} = require('../services')

//repositores
const {MovieRepository} = require('../dal/repositories')

//db
const db = require('../dal/entities');

//business
const {MovieBusiness} = require('../domain/');

const container = createContainer();

container.register({
    app: asClass(StartUp).singleton(),
    server: asClass(Server).singleton(),
    MovieController: asClass(MovieController).singleton(),
    MovieRoutes: asFunction(MovieRoutes).singleton(),
    logger: asValue(logger)
})
.register({
    router: asFunction(Routes).singleton()
})
.register({
    config: asValue(config)
})
.register({
    MovieService: asClass(MovieServices).singleton()
})
.register({
    MovieBusiness: asClass(MovieBusiness).singleton()
})
.register({
    MovieRepository: asClass(MovieRepository).singleton()
}).register({
    db: asValue(db)
});


module.exports = container;