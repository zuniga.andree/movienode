const Router = require("koa-router");
const { withTitle, withPagination, bodyFindReplace } = require("../middleware/")

module.exports = function ({ MovieController, logger }) {
    const router = new Router();

    router.get('/all', async ctx => {
        try {
            const pageSelect = await withPagination.validateAsync({ page: ctx.header.page })
            const listaMovies = await MovieController.getMovies(pageSelect)
            if(listaMovies.docs.length===0) {
                ctx.body = {detail: "No hay peliculas registradas"}
                ctx.response.status=204
            }else ctx.body = listaMovies
            
        } catch (error) {
            logger.error(error)
            if (error.name === 'ValidationError') {
                ctx.response.status = 412
                ctx.body = { detail: error.message }
            } else {
                ctx.body = { detail: error.message }
                ctx.response.status = 400
            }
        }
    });

    router.get('/title/:title', async ctx => {
        try {
            const result = await withTitle.validateAsync({ title: ctx.params.title, year: ctx.header.year })

            const movie = await MovieController.getMovie(result)

            if(movie===null) {
                ctx.body = {detail: "No se encontró ninguna pelicula"}
                ctx.response.status=204
            }else ctx.body = movie
        } catch (error) {
            logger.error(error)
            if (error.name === 'ValidationError') {
                ctx.response.status = 412
                ctx.body = { detail: error.message }
            } else {
                ctx.body = { detail: error.message }
                ctx.response.status = 400
            }
        }

    });

    router.post('/', async ctx => {
        try {
            ctx.body = await MovieController.createMovie(ctx.request.body)
        } catch (error) {
            logger.error(error)
            if (error.name === 'MongoServerError' && error.code === 11000) {
                ctx.body = { detail: 'La pelicula ya esta registrada' }
                ctx.response.status = 406
            }
            else {
                ctx.body = { detail: error.message }
                ctx.response.status = 400
            }
        }
    })

    router.post('/findAndReplace', async ctx => {
        try {
            const result = bodyFindReplace.validateAsync(ctx.request.body)
            ctx.body = await MovieController.findAndReplace(ctx.request.body)
        } catch (error) {
            logger.error(error)
            if (error.name === 'ValidationError' || error.name==='MongoServerError') {
                ctx.response.status = 412
                ctx.body = { detail: 'Error validando los parametros de entrada' }
            }
            else {
                ctx.body = { detail: error.message }
                ctx.response.status = 400
            }
        }
    })

    return router;
}