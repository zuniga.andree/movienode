const Router = require('koa-router');
const {koaSwagger} = require('koa2-swagger-ui');

const spec = require('../../config/swagger.js')

const yamljs = require('yamljs');


module.exports = function({MovieRoutes}){
    const router = new Router();
    const apiRoute = new Router();
    

    router.get('/movie/docs', koaSwagger({ routePrefix: false, swaggerOptions: { spec } }));
    apiRoute.use("/movie", MovieRoutes.routes());
    router.use("/api",apiRoute.routes());

    return router;
}