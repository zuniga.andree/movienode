module.exports={
    withTitle: require("./withTitle"),
    withPagination: require("./withPagination"),
    bodyFindReplace: require("./bodyFindReplace")
}