const Joi = require('joi');

const withPagination = Joi.object({
    page: Joi.number()
        .integer()
        .default(0)

})

module.exports = withPagination
