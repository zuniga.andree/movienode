const Joi = require('joi');

const withTitle = Joi.object({
    title: Joi.string()
        .required(),

    year: Joi.number()
        .integer()
})

module.exports = withTitle