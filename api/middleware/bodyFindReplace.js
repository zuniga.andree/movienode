const Joi = require('joi');

const bodyFindReplace = Joi.object({
    movie: Joi.string()
            .required(),

    find: Joi.string()
            .required(),

    replace: Joi.string()
            .required()
})

module.exports = bodyFindReplace