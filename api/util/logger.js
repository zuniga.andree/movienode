const {createLogger,format,transports}=require("winston")

module.exports=createLogger({
    format: format.combine(
        format.simple(),
        format.timestamp(),
        format.printf(info=>`[${info.timestamp}] ${info.level}: ${info.message}`)
    ),
    transports:[
        new transports.File({
            maxFiles: 5,
            maxsize: 5120000,
            filename: `${__dirname}/../logs/log-api.log`
        }),
        new transports.Console({
            level: 'debug'
        })
    ]
})