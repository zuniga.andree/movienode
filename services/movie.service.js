class MovieService {
    constructor({ MovieBusiness }) {
        this._movieBusiness = MovieBusiness;
    }
    getMovies(pageSelect) {        
        return this._movieBusiness.getMovies(pageSelect);;
    }

    getMovie(params){        
        return this._movieBusiness.getMovie(params);
    }

    createMovie(movie) {    
        return this._movieBusiness.createMovie(movie);;
    }

    findAndReplace(movieReplace) {    
        return this._movieBusiness.findAndReplace(movieReplace);;
    }
}

module.exports = MovieService;