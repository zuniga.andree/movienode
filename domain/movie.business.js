const { toDomainEntity } = require('./mappers')
const {toDbEntity} = require("../dal/mappers");


class MovieBusiness{
    constructor ({MovieRepository}){
        this._movieReporitory = MovieRepository;
    }

    async getMovies(pageSelect) {
        const movies = await this._movieReporitory.getMovies(pageSelect);
        movies.docs = movies.docs.map(toDomainEntity)
        //return movies.map(toDomainEntity);
        return movies;
    }

    async getMovie(params){
        const {title, year} = params
        let filter={}
        filter.title={ $regex: title, $options:'i' }
        if(year!==undefined){
            filter.year=year
        }
        const movie = await this._movieReporitory.getMovie(filter);
        if(movie===null){
            return null
        }
        return toDomainEntity(movie);
    }

    async createMovie(movie) {
        movie = toDbEntity(movie);
        return await this._movieReporitory.createMovie(movie);
    }

    async findAndReplace(movieReplace) {
        const {movie, find, replace} = movieReplace
        let filter={}
        filter.title={ $regex: movie, $options:'i' }

        const {plot} = await this._movieReporitory.getMovie(filter);
        console.log(plot)
        
        return plot.replace(find,replace);
    }
}



module.exports = MovieBusiness;