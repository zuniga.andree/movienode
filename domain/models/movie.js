const {attributes}=require("structure");

const Movie = attributes({
        title:{
            type: String,
            required: true
        },
        year:{
            type: Number,
            required: true
        }, 
        released:{
            type: Date
        },
        genre:{
            type: String
        },
        director:{
            type: String
        },
        actors:{
            type: String
        },
        plot:{
            type: String
        },
        ratings:{
            type: Array
        }
    }
)(class Movie{});

module.exports=Movie;