const Movie  = require("../models/movie");

module.exports = {
    toDomainEntity(movie) {
        const { title, year, released, genre, director, actors, plot, ratings } = movie;
        return new Movie({ title, year, released, genre, director, actors, plot, ratings })
    }
}