# MovieNode Challenge!

Desarrollo del desafio MovieNode Challenge, en el cual propone constriuir con node js un microservicio con 3 endpoint para la búsqueda de información de películas.

## Instalación

Se a preparado una imagen de docker que contine los servicios de NODE JS y MONGO DB.

### Requisitos

- Docker:

### Pasos
1. Clonar el repositorio.

```sh
git clone https://gitlab.com/zuniga.andree/movienode.git
```

2. Dentro de la raiz del repositorio clonado ejecutar en consola el siguiente comando:

```sh
docker-compose up
```

## Desarrollo del desafio

### Arquitectura

Para facilitar el desarrollo del desafio, se realiza la implemtación sobre una arquitectura limpia, separando por capas para obtener un bajo acoplamiento y mantenibilidad del codigo.

<img src="arquitectura.jpg" width="400">

### Manejo de errores

Cuando se genra un error, la aplicación guarda un historial en la siguiente ruta: `/api/logs/`

### Package utilizados:

- wilix:
- dotenv
- joi
- koa
- koa-bodyparser
- koa-router
- koa2-cors
- koa2-swagger-ui
- mongoose
- mongoose-paginate-v2
- structure
- swagger-jsdoc
- winston
- yamljs

### EndPoint

- `/api/movie/all`          Lista todos las peliculas.
- `/api/movie/title/soul`   Busca una pelicula por titulo y año.
- `/api/movie/findAndReplace`  Busca una pelicula y muesta el valor del campo splot, reemplazando su continedo segun unos paremtros de reemplazo.

#### Documentación

Para consultar la documentación, una vez se ejecute el comando `docker-compose up`, ingresar a la ruta  `http://localhost:5000/movie/docs` donde podra acceder a la información detallada de cada end point.
